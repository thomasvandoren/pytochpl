// Infinite ranges
var ns = 1..; // yields from one to infinity: 1, 2, 3, 4, 5, ...
    ns = ..1; // yields from infinity to one: ..., -5, -4, -3 , -2, -1, 0, 1
    ns = .. ; // yields from infinity to infinity: ... , ...
