#

# Values in ascending order
ns = xrange(1, 10, 1) # yields 1, 2, 3, 4, 5, 6, 7, 8, 9
ns = xrange(1, 10, 2) # yields 1, 3, 5, 7, 9

# Values in descending order
ns = xrange(9, 0, -1) # yields 9, 8, 7, 6, 5, 4, 3, 2, 1
ns = xrange(9, 0, -2) # yields 9, 7, 5, 3, 1