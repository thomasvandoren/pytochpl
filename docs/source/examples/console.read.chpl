writeln("The Answer to the Ultimate Question of Life, the Universe, and Everything, is?");
var first_answer = read(int); // Read an line of input and store it in 'answer'

writeln("That is ", first_answer == 42);

writeln("And what is the name of the biological computer?");
var second_answer = read(string);

writeln("That is ", second_answer == "Earth");
