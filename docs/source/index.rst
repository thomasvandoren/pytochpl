.. Chapel for Python Programmers documentation master file, created by
   sphinx-quickstart on Sun Jun 22 21:40:06 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Chapel for Python Programmers
=============================

Subtitle: How I Learned to Stop Worrying and Love the Curlybracket.

So, what is Chapel and why should u care? We all know that Python is the best thing since sliced bread. Python comes with batteries included and there is nothing that can't be expressed with Python in a short, concise, elegant, and easily readable manner. But, if you find yourself using any of these packages: Bohrium, Cython, distarray, mpi4py, multithreading, multiprocessing, NumPy, Numba, and/or NumExpr. You might have done so because you felt that Python's batteries needed a recharge.

You might also have started venturing deeper into the world of the world of curlybrackets. Implementing low-level methods in C/C++ and binding them to Python. In the process you might have felt that you gained performance but lost your productivity. However, there is an alternative, it does have curlybrackets, but you won't get cut on the corners.

The alternative is Chapel, it comes with a set of turbo-charged batteries for expressing parallelism, communication, and thereby providing performance! If such matters are concerns of yours, and you enjoy a nice clean syntax, then you might start caring about Chapel.


.. toctree::
   :maxdepth: 2

   getting_started

.. toctree::
   :maxdepth: 2
   
   basics

.. toctree::
   :maxdepth: 2

   parallelism

.. toctree::
   :maxdepth: 2

   numpy

.. toctree::
   :maxdepth: 2

   batteries

.. toctree::
   :maxdepth: 2

   misc

.. toctree::
   :maxdepth: 2

   keywords

.. toctree::
   :maxdepth: 2

   pythonic

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Appendix
========

.. toctree::
   :maxdepth: 2

   appendix

Bibliography
============

.. bibliography:: refs.bib

