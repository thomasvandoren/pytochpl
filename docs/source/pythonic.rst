Pythonic Module
===============

For those transitioning from Python, curious about Chapel, the ``Pythonic`` module might be a nice to take a look at. It contains a set of helper functions mimicing the functionality of some of the functions built into Python such as ``enumerate``, ``xrange``, ``range``, anong others.

If it is usefull it should probably be made available in a more convenient form, than this.

 .. literalinclude:: /playground/pythonic.chpl
    :language: c

