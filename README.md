# README #

Repository backing: chapel-for-python-programmers.readthedocs.org

### What is this repository for? ###

* Repository for "Chapel for Python Programmers"
* Work in Progress...

### How do I get set up? ###

* Install sphinxdoc (http://sphinx-doc.org/) and the Python packages pybtex and sphinxcontrib-bibtex

* Create an account on https://readthedocs.org/
* Send me an email

### Contribution guidelines ###

* Your Chapel experiences from a Python perspective
* Verification of semantics and descriptions
* Feel free to join in!

### Who do I talk to? ###

* Repo owner or admin